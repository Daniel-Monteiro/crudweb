package model.dao;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.bean.Pessoa;
import util.ConectorBanco;

public class PessoaDao {

	PreparedStatement ps;
	ResultSet rs;

	private Connection connection;

	public PessoaDao() {
		connection = ConectorBanco.getConnection();
	}

	public void inserePessoa(Pessoa p) {

		String sql = "INSERT INTO pessoa (nome,idade,salario) values(?,?,?)";

		try {

			PreparedStatement stmt = connection.prepareStatement(sql);

			stmt.setString(1, p.getNome());
			stmt.setInt(2, p.getIdade());
			stmt.setFloat(3, p.getSalario());

			stmt.execute();
			stmt.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public List<Pessoa> retornoListaPessoa() {

		List<Pessoa> lista = new ArrayList<>();

		ResultSet set;

		try {
			String sql = "SELECT * FROM pessoa";
			PreparedStatement stmt = connection.prepareStatement(sql);
			set = stmt.executeQuery();

			while (set.next()) {
				Pessoa p = new Pessoa();
				p.setCod(set.getInt("cod"));
				p.setIdade(set.getInt("idade"));
				p.setNome(set.getString("nome"));
				p.setSalario(set.getFloat("salario"));
				lista.add(p);
			}
			stmt.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lista;
	}

	public Pessoa bucaPorCod(int cod) {

		String sql = "SELECT * FROM pessoa WHERE cod=?";
		try {

			ps = connection.prepareStatement(sql);
			ps.setInt(1, cod);
			rs = ps.executeQuery();
			if (rs.next()) {
				Pessoa p = new Pessoa();
				p.setCod(rs.getInt("cod"));
				p.setIdade(rs.getInt("idade"));
				p.setNome(rs.getString("nome"));
				p.setSalario(rs.getFloat("salario"));

				return p;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	public void deletaPessoa(int id) {

		String sql = "DELETE FROM pessoa where cod = ?";

		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setInt(1, id);
			stmt.executeUpdate();
			stmt.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void atualizaPessoa(Pessoa p) {

		String sql = "Update pessoa set nome = ?, idade = ?, salario = ? where cod = ?";

		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setString(1, p.getNome());
			stmt.setInt(2, p.getIdade());
			stmt.setFloat(3, p.getSalario());
			stmt.setInt(4, p.getCod());
			stmt.executeUpdate();
			stmt.close();
		} catch (Exception e) {
			System.out.println("Erro: "+e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void salvar(Pessoa pessoa) {
		if(pessoa.getCod() != 0) {
			 atualizaPessoa(pessoa);
		}else {
			inserePessoa(pessoa);
		}
	}

	public void openConnection() {
		connection = ConectorBanco.getConnection();
	}

	public void closeConnection() {
		try {
			connection.close();
			connection = null;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static void main(String[] args) {
		PessoaDao pd =  new PessoaDao();
		System.out.println(pd.bucaPorCod(11).getNome());
	}
}