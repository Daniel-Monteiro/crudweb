package controller;

import java.io.IOException;

import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bean.Pessoa;
import model.dao.PessoaDao;


@WebServlet("/pesscontroller.do")
public class PessoaController extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
  
  
    public PessoaController() {
        super();

    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Sou o GET");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String nome = request.getParameter("nome");
		String salario = request.getParameter("salario");
		String idade = request.getParameter("idade");
		
		Pessoa p = new Pessoa();
		p.setNome(nome);
		p.setIdade(Integer.parseInt(idade));
		p.setSalario(Float.parseFloat(salario));
		
		System.out.println(p.getNome()+" "+p.getIdade()+" "+p.getSalario());
		
		try {
			PessoaDao pd = new PessoaDao();
			pd.inserePessoa(p);
			PrintWriter out = response.getWriter();
			out.println("Cadastrado com Sucesso!");
		} catch (Exception e) {
			e.printStackTrace();
		}	
		
	}

}
