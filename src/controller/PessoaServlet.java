package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bean.Pessoa;
import model.dao.PessoaDao;

@WebServlet("/PessoaServlet")
public class PessoaServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private PessoaDao pd = new PessoaDao();

	public PessoaServlet() {
		super();
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		String action = req.getParameter("action");

		int cod = Integer.parseInt(req.getParameter("cod"));

		if (action.equals(null)) {
			RequestDispatcher go = req.getRequestDispatcher("listaPessoa.jsp");
			go.forward(req, resp);
		} else if (action.equals("del")) {
			pd.deletaPessoa(cod);
			RequestDispatcher go = req.getRequestDispatcher("listaPessoa.jsp");
			go.forward(req, resp);
		} else if (action.equals("att")) {
			req.setAttribute("Pessoa", pd.bucaPorCod(cod));
			RequestDispatcher go = req.getRequestDispatcher("atualizaPessoa.jsp");
			go.forward(req, resp);
		} else {
			PrintWriter out = resp.getWriter();
			out.println("Algo Inesperado Aconteceu :( Tente Novamente Mais Tarde !");
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

				Pessoa p = new Pessoa();
				String nome = request.getParameter("nome");
				String salario = request.getParameter("salario");
				String idade = request.getParameter("idade");
				p.setNome(nome);
				p.setIdade(Integer.parseInt(idade));
				p.setSalario(Float.parseFloat(salario));
				pd.inserePessoa(p);
				System.out.println("Salvo..");
				RequestDispatcher go = request.getRequestDispatcher("index.jsp");
				go.forward(request, response);
		}
	}
