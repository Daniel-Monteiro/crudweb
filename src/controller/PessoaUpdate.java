package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bean.Pessoa;
import model.dao.PessoaDao;

/**
 * Servlet implementation class PessoaUpdate
 */
@WebServlet("/PessoaUpdate")
public class PessoaUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private PessoaDao pd = new PessoaDao();  
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PessoaUpdate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Pessoa p = new Pessoa();
	
		String nome = request.getParameter("nome");
		String salario = request.getParameter("salario");
		String idade = request.getParameter("idade");
		int cod = Integer.parseInt(request.getParameter("cod"));
		p.setCod(cod);
		p.setNome(nome);
		p.setIdade(Integer.parseInt(idade));
		p.setSalario(Float.parseFloat(salario));
		pd.atualizaPessoa(p);
		System.out.println("Atualizado..");
		RequestDispatcher go = request.getRequestDispatcher("listaPessoa.jsp");
		go.forward(request, response);
	}

}
