package util;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConectorBanco {

	private final static String URL = "jdbc:postgresql://localhost:5432/CRUD_web_Pessoa";
	private final static String User = "postgres";
	private final static  String Password = "system";

	public static Connection getConnection() {

		Connection con = null;
		
		try {
			Class.forName("org.postgresql.Driver");
			con = DriverManager.getConnection(URL, User, Password);
			System.out.println("Conectado...");
			return con;
		} catch (Exception e) {
			System.out.println("Falha ao conectar: "+ e.getMessage());
		}
		return null;
	}
	
	public static void main (String [] args) {
		ConectorBanco con = new ConectorBanco();
		con.getConnection();
	}
}