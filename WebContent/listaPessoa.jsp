<%@page import="model.dao.PessoaDao"%>
<%@page import="java.util.List" %>
<%@page import="model.bean.Pessoa" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h3>Lista Pessoas</h3>

	<table border="3">
		<tr>
			<th>Cod</th>
			<th>Nome</th>
			<th>Idade</th>
			<th>Sal�rio</th>
			<th colspan="2">A��o</th>
		</tr>
			
	<%  PessoaDao pd = new PessoaDao();
		List<Pessoa> lista = pd.retornoListaPessoa();
		
		for(Pessoa p : lista){%>
			<tr>
				<td><%= p.getCod() %></td>
				<td><%= p.getNome()%></td>
				<td><%= p.getIdade()%></td>
				<td><%= p.getSalario()%></td>
				<td><a href="PessoaServlet?action=del&cod=<%=p.getCod()%>">Excluir</a></td>
				<td><a href="PessoaServlet?action=att&cod=<%=p.getCod()%>">Atualizar</a></td>
			</tr>
		<% }%>
	</table>
	<br>
	<a href="index.jsp">Voltar <== </a>
</body>
</html>